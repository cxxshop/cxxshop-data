

using namespace nstd;

template<class... Dimension> impl::TensorImpl::TensorImpl(Dimension...&& args)
{
    data::Array dimension={std::forward<Dimension>(args)...};

}