#ifndef TENSOR_H
#define TENSOR_H

#include "api/data.h"

namespace nstd::impl
{
template <class T> class TensorImpl
{
  public:
    TensorImpl();
    template<class... Dimension> TensorImpl(Dimension...&&);
    ~TensorImpl();


};
} // namespace nstd::impl

#endif // TENSOR_H