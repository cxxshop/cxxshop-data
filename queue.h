#ifndef QUEUE_H
#define QUEUE_H

namespace nstd::impl
{
template <class T> class QueueImpl
{
  public:
    QueueImpl();
    ~QueueImpl();
};
} // namespace nstd::impl

#endif // QUEUE_H