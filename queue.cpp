#include "data/queue.h"
#include "api/data.h"

using namespace nstd;

/* Queue */
template <class T> data::Queue<T>::Queue() : m_impl(new impl::QueueImpl<T>()) {}
template <class T>
data::Queue<T>::Queue(const data::Queue<T> &q) : m_impl(new impl::QueueImpl<T>(q))
{
}

template <class T>
data::Queue<T>::Queue(data::Queue<T> &&q)
    : m_impl(new impl::QueueImpl<T>(std::forward<data::Queue<T>>(q)))
{
}

template <class T>
data::Queue<T>::Queue(std::initializer_list<T> list)
    : m_impl(new impl::QueueImpl<T>(std::forward<std::initializer_list<T>>(list)))
{
}

template <class T> data::Queue<T>::~Queue() { delete m_impl; }
template <class T> data::Queue<T> &data::Queue<T>::operator=(const data::Queue<T> &) {}
template <class T> data::Queue<T> &data::Queue<T>::operator=(data::Queue<T> &&) {}
template <class T> bool data::Queue<T>::operator==(const data::Queue<T> &) {}
template <class T> bool data::Queue<T>::operator!=(const data::Queue<T> &) {}
template <class T> T data::Queue<T>::pop() { return m_impl->pop(); }

template <class T> data::Queue<T> &data::Queue<T>::push(T &&item)
{
    m_impl->push(std::forward<T>(item));
    return *this;
}

template <class... Args, class T> data::Queue<T> &data::Queue<T>::emplace(Args... &&args)
{
    m_impl->emplace(std::forward<Args>(args)...);
    return *this;
}

template <class T> data::Queue<T> &data::Queue<T>::for_each(std::function<void(const T &)> callback)
{
}

template <class T> T data::Queue<T>::front() const {}
template <class T> T data::Queue<T>::back() const {}
template <class T> data::ull data::Queue<T>::size() const {}

/* QueueImpl */

template <class T> impl::QueueImpl<T>::QueueImpl() {}
