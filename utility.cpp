#include "api/data.h"

#include <iostream>

using namespace nstd;

template <class T> int data::print(const data::Queue<T>& q)
{
    std::cout<<"Queue { size = "<<q.size()<<" } : [ ";
    q.for_each([](const T& item){std::cout<<item<<", ";});
    std::cout<<" ]"<<std::endl;
}

template <class T> int data::print(const data::Stack<T>& s)
{
    std::cout<<"Stack { size = "<<s.size()<<" } : [ ";
    s.for_each([](const T& item){std::cout<<item<<", ";});
    std::cout<<" ]"<<std::endl;
}

template <class T> int data::print(const data::Array<T>& a)
{
    std::cout<<"Array { size = "<<a.size()<<" } : [ ";
    a.for_each([](const T& item){std::cout<<item<<", ";});
    std::cout<<" ]"<<std::endl;
}
